Este proyecto fue creado con [Express Generator](https://expressjs.com/es/starter/generator.html).

Para instalar las dependencias del proyecto se debe ejecutar el comando: `npm install`

## Scripts Disponibles

En el directorio del proyecto, se pueden ejecutar:

### `npm start`

Ejecuta la aplicación en modo desarrollo.<br>

- Abrir [http://localhost:3001](http://localhost:3000) para ver en el navegador.
- Hacer una petición post a la dirección http://localhost:3001/login, con los valores `username` y `password` encriptados para autentificar a un usuario.

NOTA: Para que el sistema funcione adecuadamente la variable KEY del archivo .env debe tener el mismo valor que la variable PEC3_KEY del archivo .env del proyecto `p3-front-end`

