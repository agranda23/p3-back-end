var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('PEC3 - Programación de Código Seguro');
});

module.exports = router;
