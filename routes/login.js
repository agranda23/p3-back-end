var express = require('express');
var router = express.Router();
var userHelper = require('../common/usersList.js');

router.get('/', function(req, res, next) {
  res.send('Para autentificar usuarios debe hacer una solicitud POST a la url actual');
});

var getLoginResponse = function(user) {
  if (user === null) {
    return {'authenticated': false, 'message': 'Ocurrió un error al consultar la base de datos'};
  }

  if (!user) {
    return {'authenticated': false, 'message': 'Credenciales inválidas'};
  }

  return {'authenticated': true, 'fullName': `${user.name} ${user.lastname}`, 'username': user.user};
};

router.post('/', function(req, res, next) {
  var username = req.body.username;
  var password = req.body.password;
  var user = userHelper.getUser(username, password);
  var response = getLoginResponse(user);

  res.json(response);
});

module.exports = router;
