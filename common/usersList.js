var cryptography = require('./utils/cryptography');

var users = [{
  'username': 'german',
  'name': 'Germán',
  'lastname': 'Ramírez',
  'password': 'd80f14866a27d1751017148064c9e2bd69249d05199b4460b0b6303791bad04a',
}, {
  'username': 'gina',
  'name': 'Gina',
  'lastname': 'Palma',
  'password': '33becebe64861170a43a1c5a4e8afe913aed501d0e86b1b539c569b25835491f',
}, {
  'username': 'antonio',
  'name': 'Antonio',
  'lastname': 'Robalino',
  'password': 'ee16552835a93946f80afe2924a2ccf21080a742c748e8a292838fe976608344',
}, {
  'username': 'raul',
  'name': 'Raúl',
  'lastname': 'Córdova',
  'password': 'cbb65dce086c687e80c5471f167956a31d22be1882c40994ede60ee16284a2fd',
}, {
  'username': 'viviana',
  'name': 'Viviana',
  'lastname': 'Ramón',
  'password': '218dd8e8d92bf1111fdb9c0b05339f6f71e9cc747ea8488297c3af38f5e7649d',
}];

var checkValues = function(username, password) {
  if (!username || !password || !cryptography.decrypt(username) || !cryptography.decrypt(password)) {
    return false;
  }

  return true;
};

exports.getUser = function(username, password) {
  if (!checkValues(username, password)) {
    return;
  }

  var plainUsername = cryptography.decrypt(username);
  var hashedPassword = cryptography.getHash(cryptography.decrypt(password));

  return users.find((user) => user.username === plainUsername && user.password === hashedPassword);
};
