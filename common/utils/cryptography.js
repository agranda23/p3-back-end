require('dotenv').config();

var key = process.env.KEY;
var encryptor = require('simple-encryptor')(key);

exports.getHash = function(plainText) {
  return encryptor.hmac(plainText);
};

exports.encrypt = function(plainText) {
  return encryptor.encrypt(plainText);
};

exports.decrypt = function(encryptedText) {
  return encryptor.decrypt(encryptedText);
};
